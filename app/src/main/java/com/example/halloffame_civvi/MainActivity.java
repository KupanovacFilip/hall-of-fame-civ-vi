package com.example.halloffame_civvi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView ivTeddy;
    ImageView ivGandhi;
    ImageView ivBarbarossa;
    TextView tvTeddy;
    TextView tvGandhi;
    TextView tvBarbarossa;
    Button btnInspiration;
    Button btnEditDesc;
    EditText etDescription;
    RadioButton rbTeddy;
    RadioButton rbGandhi;
    RadioButton rbBarbarossa;

    String teddy="I don't pity any man who does hard work worth doing. I admire him.";
    String mahatmaGandhi="A man is but a product of his thoughts. What he thinks he becomes.";
    String barbarossa="It is not for the people to give laws to the prince, but to obey his mandate.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();
    }

    private void initializeUI(){

        this.ivTeddy=(ImageView)findViewById(R.id.ivTeddyRoose);
        this.ivGandhi=(ImageView)findViewById(R.id.ivGandhi);
        this.ivBarbarossa=(ImageView)findViewById(R.id.ivBarbarossa);
        this.tvTeddy=(TextView)findViewById(R.id.tvDescTeddy);
        this.tvGandhi=(TextView)findViewById(R.id.tvDescGandhi);
        this.tvBarbarossa=(TextView)findViewById(R.id.tvDescBarbarossa);
        this.btnEditDesc=(Button)findViewById(R.id.btnEditDesc);
        this.btnInspiration=(Button)findViewById(R.id.btnInspiration);
        this.etDescription=(EditText)findViewById(R.id.etEnterNewDesc);
        this.rbTeddy=(RadioButton)findViewById(R.id.rbTeddy);
        this.rbGandhi=(RadioButton)findViewById(R.id.rbGandhi);
        this.rbBarbarossa=(RadioButton)findViewById(R.id.rbBarbarossa);
        Random random = new Random();

        this.ivTeddy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivTeddy.setVisibility(View.GONE);
            }
        });
        this.ivGandhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    ivGandhi.setVisibility(View.GONE);
            }
        });
        this.ivBarbarossa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBarbarossa.setVisibility(View.GONE);
            }
        });

        this.btnInspiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nexInsp=random.nextInt(3);
                if(nexInsp==0) {
                    DisplayToast(teddy);
                }
                else if(nexInsp==1) {
                    DisplayToast(mahatmaGandhi);
                }
                else {
                    DisplayToast(barbarossa);
                }
            }
        });
        this.btnEditDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rbTeddy.isChecked()){
                    tvTeddy.setText(etDescription.getText().toString());
                }
                if(rbGandhi.isChecked()){
                    tvGandhi.setText(etDescription.getText().toString());
                }
                if(rbBarbarossa.isChecked())
                    tvBarbarossa.setText(etDescription.getText().toString());

            }
        });
    }
    private void DisplayToast(String inspiration){
        Toast.makeText(this, inspiration, Toast.LENGTH_LONG).show();
    }
}